<?php
/**
 * @file
 * Contains the views handler to list the book vocab's terms.
 */

class views_handler_field_primary_vocab_terms extends views_handler_field_prerender_list {
  function init(&$view, $options) {
    parent::init($view, $options);
    if ($view->base_table == 'node_revisions') {
      $this->additional_fields['vid'] = array('table' => 'node_revisions', 'field' => 'vid');
    }
    else {
      $this->additional_fields['vid'] = array('table' => 'node', 'field' => 'vid');
    }
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['link_to_taxonomy'] = array('default' => TRUE);

    return $options;
  }

  /**
   * Provide "link to term" option.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_taxonomy'] = array(
      '#title' => t('Link this field to its term page'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_taxonomy']),
    );
  }

  /**
   * Add this term to the query
   */
  function query() {
    $this->add_additional_fields();
  }

  function pre_render($values) {
    $this->field_alias = $this->aliases['vid'];
    $vids = array();
    foreach ($values as $result) {
      $vids[] = $result->{$this->aliases['vid']};
    }

    if ($vids) {
	    foreach ($vids as $vid) {
		    $terms = taxonomy_get_tree($vid);
		    foreach($terms as $term) {
			    if (empty($this->options['link_to_taxonomy'])) {
	          $this->items[$term->vid][$term->tid] = check_plain($term->name);
	        }
	        else {
	          $this->items[$term->vid][$term->tid] = l($term->name, taxonomy_term_path($term));
	        }
		    }
	    }    
    }
  }
}

