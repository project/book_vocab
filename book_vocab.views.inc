<?php

/**
 * Implementation of hook_views_data()
 */
function book_vocab_views_data() {	
  $data = array();

  $data['book_vocab']['table']['group']  = t('Vocabulary per-Book');

  $data['book_vocab']['table']['join'] = array(
	  'node' => array(
		  'left_table' => 'book',
		  'left_field' => 'bid',
		  'field' => 'bid',
		),
  );

  $data['book_vocab']['vid'] = array(
    'title' => t('Terms for primary vocabulary'), // The item it appears as on the UI,
    'field' => array(
      'help' => t('Displays terms from the primary vocabulary of a book.'),
      'handler' => 'views_handler_field_primary_vocab_terms',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function book_vocab_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'book_vocab'),
    ),
    'handlers' => array(
      // field handlers
      'views_handler_field_primary_vocab_terms' => array(
        'parent' => 'views_handler_field_prerender_list',
      ),
    ),
  );
}